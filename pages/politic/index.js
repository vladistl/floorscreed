
import { useState } from "react";

import MainLayout from "/components/layouts/MainLayout";
import Footer from "/components/footer/Footer";
import MinimalHeader from "/components/header/MinimalHeader";
import FormatText from '../../components/FormatText';


  export default function PoliticPage({dataPage}) {

    
    const {common_data, meta, content} =  dataPage

    const [modalActive, setModalActive] = useState(false)


    const phone = common_data.phone 

    const menu = common_data.menu || []


    const contentTitle = meta.h1

    const argsHeader = {
      menu, phone, contentTitle, setModalActive
    }

    const argsFooter = {
      menu, phone, modalActive, setModalActive,  scripts: common_data.scripts
    }



  const mainArgs = {
    title: meta.title,
    description: meta.description,
    keywords: meta.keywords,
    wrapClassName : "is-politic-page",
    scripts: common_data.scripts
  }


    return (
      <>
      <MainLayout {...mainArgs}>
        <MinimalHeader {...argsHeader}/>
        <FormatText text={content.text}/>
        <Footer {...argsFooter}/>
      </MainLayout>
      </>
    )
}

export async function getStaticProps(context) {
  const res = await fetch(`https://api.styazhka-pola23.ru/wp-json/routes/data-page/politicPage`)
  const dataPage = await res.json()

  if (!dataPage) {
    return {
      notFound: true,
    }
  }

  return {
    props: { dataPage }, // will be passed to the page component as props
  }
}
