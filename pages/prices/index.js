
import { useState } from "react";

import MainLayout from "/components/layouts/MainLayout";
import SubmitApplication from '/components/SubmitApplication';
import Footer from "/components/footer/Footer";
import MinimalHeader from "/components/header/MinimalHeader";
import PriceList from '/components/PriceList';



  export default function PricesPage({dataPage}) {

    const {common_data, meta, content} =  dataPage

    const [modalActive, setModalActive] = useState(false)

    const phone = common_data.phone || `+7 (918) 657-35-64`


    
    const menu = common_data.menu || []

  const contentTitle = meta.h1

  const argsHeader = {
    menu, phone, contentTitle, setModalActive
  }

  const argsFooter = {
    menu, phone, modalActive, setModalActive, scripts: common_data.scripts
  }


  const mainArgs = {
    title: meta.title,
    description: meta.description,
    keywords: meta.keywords,
    wrapClassName : "is-prices-page",
    scripts: common_data.scripts
  }


    return (
      <>
      <MainLayout {...mainArgs}>
        <MinimalHeader {...argsHeader}/>

        <PriceList prices={content.price}/>

        <SubmitApplication />
            
        <Footer {...argsFooter}/>
      </MainLayout>
      </>
    )
}


export async function getStaticProps(context) {
  const res = await fetch(`https://api.styazhka-pola23.ru/wp-json/routes/data-page/pricePage`)
  const dataPage = await res.json()

  if (!dataPage) {
    return {
      notFound: true,
    }
  }

  return {
    props: { dataPage }, // will be passed to the page component as props
  }
}
