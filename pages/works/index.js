
import { useState } from "react";

import MainLayout from "/components/layouts/MainLayout";
import Footer from "/components/footer/Footer";
import MinimalHeader from "/components/header/MinimalHeader";
import CustomImageList from '/components/CustomImageList'



  export default function WorksPage({dataPage}) {

    const [modalActive, setModalActive] = useState(false)

    const {common_data, meta, content} =  dataPage

    const phone = common_data.phone 

    const menu = common_data.menu || []


    const contentTitle = meta.h1

    const argsHeader = {
      menu, phone, contentTitle, setModalActive
    }
  
    const argsFooter = {
      menu, phone, modalActive, setModalActive, scripts: common_data.scripts
    }
  
  
    const mainArgs = {
      title: meta.title,
      description: meta.description,
      keywords: meta.keywords,
      wrapClassName : "is-prices-page",
      scripts: common_data.scripts
    }

    const workGallery = content.work_gallery;

    return (
      <>


      <MainLayout {...mainArgs}>
        <MinimalHeader  {...argsHeader}/>
        <div className="our-works">
        <CustomImageList gallery={workGallery} maxCountImage={60}/> 
        </div>
        <Footer {...argsFooter}/>
      </MainLayout>
      </>
    )
}


export async function getStaticProps(context) {
  const res = await fetch(`https://api.styazhka-pola23.ru/wp-json/routes/data-page/worksPage`)
  const dataPage = await res.json()


  if (!dataPage) {
    return {
      notFound: true,
    }
  }

  return {
    props: { dataPage }, // will be passed to the page component as props
  }
}