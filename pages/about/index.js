import { useRouter } from 'next/router'
import { useState } from "react";

import MainLayout from "/components/layouts/MainLayout";
import Footer from "/components/footer/Footer";
import FullHeader from "/components/header/FullHeader";
import Content from "/components/header/Content";

import bigMainImage from '/public/img/mainAboutScreen.png'
import AboutAdvatages from '/components/AboutAdvantages';
import SubmitApplication from '/components/SubmitApplication';

  export default function AboutPage({dataPage}) {

    const [modalActive, setModalActive] = useState(false)

    const {common_data, meta, main_img, content} =  dataPage

    const phone = common_data.phone || `+7 (918) 657-35-64`

    const menu = common_data.menu || []


    const contentTitle = meta.h1

    const argsHeader = {
      menu, phone, contentTitle, setModalActive
    }
  
    const argsFooter = {
      menu, phone, modalActive, setModalActive,scripts: common_data.scripts
    }
  
  
    const mainArgs = {
      title: meta.title,
      description: meta.description,
      keywords: meta.keywords,
      wrapClassName : "is-about-page",
      scripts: common_data.scripts
    }

    const mainImg = main_img.url

    const advantages = content.advantages

    return (
      <>
      <MainLayout {...mainArgs}>
        <FullHeader {...argsHeader}>
            <Content titleContent={meta.h1} img={mainImg}  setModalActive={setModalActive}/>
        </FullHeader>

        <AboutAdvatages advantages={advantages}/>

        <SubmitApplication/>

        <Footer {...argsFooter}/>

      </MainLayout>
      </>
    )
}

export async function getStaticProps(context) {
  const res = await fetch(`https://api.styazhka-pola23.ru/wp-json/routes/data-page/aboutPage`)
  const dataPage = await res.json()

  console.log(dataPage)

  if (!dataPage) {
    return {
      notFound: true,
    }
  }

  return {
    props: { dataPage }, // will be passed to the page component as props
  }
}
