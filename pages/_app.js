import '/styles/style.sass'
import NextNprogress from 'nextjs-progressbar'
import { ToastContainer, toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.min.css';

function MyApp({ Component, pageProps }) {

    return (
        <>
          <NextNprogress
            color="#FFB800"
            startPosition={0.3}
            stopDelayMs={200}
            height={3}
            showOnShallow={true}
          />
          <ToastContainer />
          <Component {...pageProps} />
        </>
      )
  }
  
export default MyApp