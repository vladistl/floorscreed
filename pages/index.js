import { useRouter } from 'next/router'
import { useState } from "react";

import MainLayout from "/components/layouts/MainLayout";
import Footer from "/components/footer/Footer";
import FullHeader from "/components/header/FullHeader";
import Content from "/components/header/Content";


import PreviewDescription from "/components/PreviewDescription";
import MainAdvatages from "/components/MainAdvantages";
import Process from "/components/Process";
import OurWorks from "/components/OurWorks";
import MainPrice from "/components/MainPrice";
import Reviews from "/components/Reviews";
import Text from "/components/Text";


  export default function HomePage({dataPage}) {

    const {common_data, meta, main_img, content} =  dataPage

    const [modalActive, setModalActive] = useState(false)

    const phone = common_data.phone || `+7 (918) 657-35-64`

    const menu = common_data.menu || []

    const argsHeader = {
      menu, phone, setModalActive
    }

    const argsFooter = {
      menu, phone, modalActive, setModalActive, scripts: common_data.scripts
    }
    console.log(content);

    const text = content.small_description;

    const description = content.description;

    const bottomTitle = content.bottom_title;

    const workGallery = content.work_gallery;

    const priceList = content.price;



    const mainImg = main_img.url

    const mainArgs = {
      title: meta.title,
      description: meta.description,
      keywords: meta.keywords,
      wrapClassName : "is-home-page",
      scripts: common_data.scripts
    }

    return (
      <>
      <MainLayout {...mainArgs}>
        <FullHeader {...argsHeader}>
            <Content titleContent={meta.h1} img={mainImg} setModalActive={setModalActive}/>
        </FullHeader>

         <PreviewDescription text={text}/>
        <MainAdvatages/>
        <Process/>
        <OurWorks gallery={workGallery}  maxCountImage={9}/>
        <MainPrice priceList={priceList}/>
        <Reviews/>
        <Text title={bottomTitle} text={description}/>
        <Footer {...argsFooter}/>
     
      </MainLayout>
      </>
    )
}

export async function getStaticProps(context) {
  const res = await fetch(`https://api.styazhka-pola23.ru/wp-json/routes/data-page/mainPage`)
  const dataPage = await res.json()


  if (!dataPage) {
    return {
      notFound: true,
    }
  }

  return {
    props: { dataPage }, // will be passed to the page component as props
  }
}