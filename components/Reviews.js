import Slider from "react-slick";
import { useState, useEffect } from "react";
import Head from 'next/head'

export default function Reviews(){

    const settings = {
        dots: true,
      speed: 500,
      infinity: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      centerPadding: 20,
      arrows: true
      };

    const widthScreen = () => {
        const { innerWidth: width, innerHeight: height } = typeof window == 'undefined' ? {width: 0, height: 0}: window;
    
        return {
            width,
            height
        };
    }

    const [showMoreBtn, setShowMoreBtn] = useState(true)
    const [reviews, setReviews] = useState([])
    const [reviewsParam, setReviewsParam] = useState({
        // per_page: 3,
        // page: 1
    })



    async function getReviews({
        per_page,
        page
    }){

        return fetch(`https://api.styazhka-pola23.ru/wp-json/wp/v2/reviews?page=${page}&per_page=${per_page}`)
        .then( response => response.json() )
        .then( responseReviews => {
            
            let prevReviews = reviews

            for(const kReview in responseReviews){
                let review = responseReviews[kReview]
                prevReviews.push({
                    name: review.title.rendered,
                    text: review.content.rendered
                })

        
            }
            return {prevReviews, page: page + 1, showBtn: !(responseReviews.length < per_page)}
   
            

        } )
          
        
        
    }

    const showMoreReviews = e =>{
        e.preventDefault()

        getReviews(reviewsParam).then( result =>{
            setTimeout(()=> {
                if(!result.showBtn) setShowMoreBtn(false)
                setReviewsParam({per_page: 3, page: result.page})
             
                setReviews(result.prevReviews)
            }, 1)
       
        })
    }

    useEffect( () => {
            const per = widthScreen().width > 728 ? 10 : 3

            getReviews({per_page: per, page: 1})
            .then( result =>{
                setTimeout(()=> {
                    if(!result.showBtn) setShowMoreBtn(false)
                    setReviewsParam({per_page: 3, page: result.page})
           
                    setReviews(result.prevReviews)
                }, 1)
           
            })

 
    }, [])

    return (
        <>
        <Head>
            {widthScreen().width > 728 ?
                <>
                    
                        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
                        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
                     
                </>
                : ""
            }
        </Head>
        
        <div className="reviews">
            <div className="container reviews--container">
                <p className="title reviews--title" >Отзывы</p>
                {widthScreen().width > 728 ?
                    <div className="reviews--wrap-slider">
                        <Slider {...settings}>
                            {reviews.map((item,index) => (
                                <div  key={index} className="reviews--item">
                                    <div className="reviews--user">
                                        {item.name}
                                    </div>
                                    <div className="reviews--text">
                                        {item.text.replace(/<\/?[^>]+(>|$)/g, "")}
                                    </div>
                                </div>
                            ))}
                           
                     
                        </Slider>
                        <div className="bottom-arrows">
                            

                        </div>
                    </div>
                : 
                    <>
                        <div className="reviews--items">

                            {reviews.map((item,index) => (
                                <div key={index} className="reviews--item">
                                    <div className="reviews--user">
                                        {item.name}
                                    </div>
                                    <div className="reviews--text">
                                        {item.text.replace(/<\/?[^>]+(>|$)/g, "")}
                                    </div>
                                </div>
                            ))}
                        </div> 
                        {showMoreBtn ?
                        <a 
                            onClick={showMoreReviews}
                            className="reviews--more">
                            <img src="/img/btn-loader.png" className="reviews--more-loader"/>
                            <span  className="reviews--more-text">Еще отзывы</span>
                        </a>
                        : ''}
                    </>
                }
            </div>
         
        </div>
    </>
    )
}