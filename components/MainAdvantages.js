export default function MainAdvatages(){

    const infoAdvantages = [
        {
            text:  `Качественное выполнение работ`,
            img: `/img/manAdvantages.png`,
            class: ``
        },
        {
            text:  `Гарантия качества`,
            img: `/img/ratingAdvantages.png`,
            class: ``
        },
        {
            text:  `Работаем по договору`,
            img: `/img/paperAdvantages.png`,
            class: ``
        }, 
        {
            text:  `Опыт более 12 лет`,
            img: `/img/ageAdvantages.png`,
            class: ``
        },
        {
            text:  `Ответственная бригада`,
            img: `/img/buildersAdvantages.png`,
            class: ``
        }

    ];

    return (
        <div className="main-advantages">
            <div className="container main-advantages--container">
                {infoAdvantages.map((item, index)=>(
                    <div key={index} className={`main-advantages--item main-advantages--item-${index+1}`}>

                        <p className="main-advantages--text">
                            {item.text}
                        </p>
                        {item.img ? <img src={item.img} className="main-advantages--img"/>:""}
                    </div>
                ))}
               

            </div>
        </div>
    )
}