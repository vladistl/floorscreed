import Form from "./Form";


export default function SubmitApplication(){
    return (
        <div className="submit-application">
                <div className="container submit-application--container">
                    <div className="submit-application--wrap">
                        <div className="submit-application--left">
                            <p className="submit-application--title">Оставьте заявку и получите <span className="yellow-text">специальное</span> <span className="yellow-text">предложение</span></p>
                            <p className="submit-application--text">Заполните форму, мы перезвоним и зафиксируем за вашим номером
скидку 25% процентов на стяжку пола</p>
                            <div className="submit-application--master">
                                <div className="submit-application--master-label">
                                    <img className="submit-application--master-img" src="/img/master.png"/>
                                </div>
                                <div className="submit-application--master-info">
                                    <div className="submit-application--master-name">
                                        Иван
                                    </div>
                                    <div className="submit-application--master-position">
                                        Главный мастер
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="submit-application--right">
                            <Form/>
                        </div>
                    </div>
                    
                </div>
        </div>
    )
}