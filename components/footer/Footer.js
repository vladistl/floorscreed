import Link from 'next/link'
import Modal from '/components/Modal';
import Form from '/components/Form';

export default function Footer ({menu, phone, modalActive, setModalActive, scripts}) {

    
    return (
        <>
            <div className="footer">
                <div className="container footer--container">
                    <div className="footer--top">
                        <Link href="/">
                            <a className="footer-logo">
                                <img
                                    src="/img/logo.png"
                                    alt="Стяжка пола 23"
                            
                                    className="footer-logo--img"
                                />
                                <span className="footer-logo--text">
                                    Стяжка пола 23
                                </span>
                            </a>
                        </Link>
                        <div className="menu">
                            {menu.map((item, index) => (
                                <Link key={index} href={item.link}>
                                    <a className={ item.isActive? "menu--item active":"menu--item"}>
                                    {item.name}
                                    </a>
                                </Link>
                            ))}
                        
                        
                        </div>
                        <div className="header-callback">
                            <a className="header-callback--phone" href={`tel:${phone}`}>{phone}</a>
                            <a className="header-callback--btn" 
                                onClick={e => {
                                    setModalActive(true)
                                }}
                            >Позвоните мне</a>
                        </div>
                    </div>
                    <div className="footer--bottom">
                        <div className="footer--bottom-item">
                            <div className="footer--copy">
                                © Стяжка полов 23, {(new Date()).getFullYear()}
                            </div> 
                            <Link href="/politic"> 
                                <a className="footer--politic">
                                    Политика конфиденциальности
                                </a>
                            </Link>      
                        </div>  
                        <div className="footer--bottom-item">
                    
                            <a href="https://gragoweb.ru" target="_blank" className="footer--developer">
                                <span className="footer--developer-text">Разработка и продвижение</span>
                                <img className="footer--developer-img" src="/img/dragoweb.png"/>
                            </a>   
                        </div>    
                    </div>
                </div>
            </div>
            <Modal  active={modalActive} setActive={setModalActive} title="Заполните форму и мы вам перезвоним">
                <Form modalActive={modalActive} setModalActive={setModalActive}/>
            </Modal>
            <script dangerouslySetInnerHTML={{ __html: scripts.footer}}></script>                   
        </>
    )
}