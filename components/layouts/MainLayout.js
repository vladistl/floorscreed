import Head from 'next/head'

export default function MainLayout({children, wrapClassName, title, description, keywords, scripts}){

    return (
        <>
       
            <Head>
                <title>{title|| "Стяжка пола 23"}</title>
                <meta name="description" content={description || "Ровная поверхность нашей стяжки позволяет вести укладку плитки, паркета, ламината без дополнительного выравнивания"}/>
                <meta name="keywords" content={keywords || "стяжка, укладка пола"}/>
                <link rel="icon" href="/favicon.ico" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />

                <meta property="og:title" content={title|| "Стяжка пола 23"}></meta>
                <meta property="og:description " content={description|| "Стяжка пола 23"}></meta>
                <script dangerouslySetInnerHTML={{ __html: scripts.header}}></script>
            </Head>
            <noscript dangerouslySetInnerHTML={{ __html: scripts.body}}></noscript>
            <div className={wrapClassName || ""}>
                {children}
            </div>



        </>
      )
}