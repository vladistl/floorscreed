import ImageList from '@material-ui/core/ImageList';
import ImageListItem from '@material-ui/core/ImageListItem';

export default function CustomImageList({gallery,maxCountImage}) {
  gallery = gallery.slice(0, maxCountImage)
  return (
        <ImageList className="image-list" cols={3}>
            {gallery.map((item, index) => (
                
                <ImageListItem key={item.url}>
                  <img
                      src={`${item.url}?w=164&h=164&fit=crop&auto=format`}
                      srcSet={`${item.url}`}
                      alt={item.title}
                      loading="lazy"
                  />
                </ImageListItem>
            ))}
        </ImageList>
  );
}

const itemData = [
 
];