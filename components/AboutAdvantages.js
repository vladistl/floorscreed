export default function AboutAdvatages({advantages}){

    const {title, items} = advantages

    return (
        <div className="about-advantages">
            <div className="container ">
            <p className="title about-advantages--title" >{title}</p>
                <div className="about-advantages--container">
                    {items.map((item, index)=>(
                        <div key={index} className={`about-advantages--item about-advantages--item-${index+1}`}>
                            {item.img ? <img src={item.img} className="about-advantages--img"/>:""}
                            <p className="about-advantages--text">
                                {item.text}
                            </p>
                            
                        </div>
                    ))}
                </div>

            </div>
        </div>
    )
}