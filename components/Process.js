import Image from 'next/image'

export default function Process(){
    return (
        <div className="process">
            <div className="container process--container">
                <p className="title process--title" >Процесс</p>

                    <div className="process--img">
                        <Image
                            src="/img/process.png"
                            width={1393}
                            height={608}
                        />
                    </div>
                    <div className="process--items">
                        <div className="process--item">
                            <span className="process--number">1</span>
                            <span className="process--text">Подготовка основания</span>
                        </div>
                        <div className="process--item">
                            <span className="process--number">2</span>
                            <span className="process--text">Подготовка и подача цементного раствора</span>
                        </div>
                        <div className="process--item">
                            <span className="process--number">3</span>
                            <span className="process--text">Выставление маяков</span>
                        </div>
                        <div className="process--item">
                            <span className="process--number">4</span>
                            <span className="process--text">Выравнивание стяжки</span>

                        </div>
                        <div className="process--item">
                            <span className="process--number">5</span>
                            <span className="process--text">Финишная затирка пола</span>

                        </div>
                        <div className="process--item">
                            <span className="process--number">6</span>
                            <span className="process--text">Высыхание пола</span>
                        </div>
                    </div>
            </div>
        </div>
    )
}