import Link from 'next/link'
import { useState } from "react";


export default function Text({title, text}){

    const [showText, setShowText] = useState(false)

    return (
        <div className="text-block">
            <div className="container text-block--container">
                <p className="title text-block--title" >{title}</p>
          
                <div className={ !showText ? "text-block--text" : "text-block--text text-block--text-active"} dangerouslySetInnerHTML={{__html:text}} />
            </div>
         
           
                <a className ="text-block--all"
                    onClick={ e => {
                        e.preventDefault()
                        setShowText(prevState => !prevState)
                    }}
                >
                    <img src="/img/btn-loader.png" className="text-block--more-loader"/>
                    {!showText ? 
                    
                        <span className="text-block--all-text">
                            Еще информация
                        </span>
                        :
                        <span className="text-block--all-text">
                            Скрыть
                        </span>
                    }
                </a>
           
        </div>
    )
}