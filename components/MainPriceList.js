import Link from 'next/link'

export default function MainPriceList({priceList}){


    return (
        <>
            <div class="main-price-list">
                <div class="main-price-list--item main-price-list--top">
                    <div class="main-price-list--name main-price-list--bold">
                        Площадь помещения
                    </div>
                    <div class="main-price-list--value main-price-list--bold">
                        Цена
                    </div>
                </div>

                {priceList.map((item, index) => (
                    <div key={index} class="main-price-list--item">
                        <div class="main-price-list--name">
                            {item.quadrature}
                        </div>
                        <div class="main-price-list--value main-price-list--bold">
                            {item.price}
                        </div>
                    </div>
                ))}

              

            </div>

            <Link href="/prices">
              
                <a className ="main-price-list--all">
                    <span className="main-price-list--all-desctop">Подробнее про цены</span>
                    <span className="main-price-list--all-mobile">Подробнее</span>
                </a>
             
            </Link>
        </> 
    )
}