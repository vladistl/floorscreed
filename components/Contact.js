
export default function Contact({contacts}){

    const {titile, description, call, phone, map, adress} = contacts

    return (
        <div className="contact">
            <div className="container contact--container">
                <p className="title contact--title">{titile}</p>
                <p className="contact--description" dangerouslySetInnerHTML={{__html:description}}></p>

                <a className="contact--phone" href={`tel:${phone}`}>{phone}</a>

                <div className="contact--feedback-items">
                    {
                        call.map((item, index) => (
                            <a key={index} className="contact--feedback-item" href={item.link}>{item.name}</a>
                        ))
                    }
                    
                </div>
                <div className="contact--map"
                    dangerouslySetInnerHTML={{__html:map}}
                >
               
                </div>
                <div className="contact--address"
                    dangerouslySetInnerHTML={{__html:adress}}
                >
                
                </div>
            </div>
        </div>
    )
}