import Link from 'next/link'
import CustomImageList from './CustomImageList'

export default function OurWorks({gallery, maxCountImage}){
    return (
        <div className="our-works">
            <div className="container our-works--container">
                <p className="title our-works--title" >Наши работы</p>
          
               
            </div>
            <CustomImageList gallery={gallery} maxCountImage={maxCountImage}/>
            <Link href="/works">
                <a className ="our-works--all">Все работы</a>
            </Link>
        </div>
    )
}