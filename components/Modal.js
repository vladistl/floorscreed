export default function Modal({active, setActive, title, children}){
    return (
        <div 
            className={active ? "modal active": "modal"}
            onClick={() => setActive(false)}
        >
            <div 
                className={active ? "modal--main-content active": "modal--main-content"}
                onClick={e => e.stopPropagation()}
            >
                <div className="modal--top">

                    <span 
                        className="modal--close" 
                        onClick={() => setActive(false)}
                    >
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28" fill="none">
                            <line x1="1.85284" y1="26.1116" x2="25.612" y2="2.35242" stroke="black" stroke-width="5"/>
                            <line y1="-2.5" x2="33.6006" y2="-2.5" transform="matrix(-0.707107 -0.707107 -0.707107 0.707107 23.7593 27.8794)" stroke="black" stroke-width="5"/>
                        </svg>
                    </span>
                    <p className="modal--title">{title || ""}</p>
                </div>

                <div className="modal--body">
                {children}
                </div>
             
            </div>
        </div>
    )
}