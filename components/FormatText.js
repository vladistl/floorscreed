export default function FormatText({text}){

    

    return (
        <div className="format-text">
            <div className="container format-text--container" dangerouslySetInnerHTML={{__html:text}}>

            </div>
        </div>
    )
}