export default function PreviewDescription({text}){
    return (
        <div className="preview-description"> 
            <div className="container preview-description--container"> 
                <p className="preview-description--text"  dangerouslySetInnerHTML={{__html:text}} />
            </div>
        </div>
    )
}