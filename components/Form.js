import { FormControlLabel, Checkbox } from '@material-ui/core';
import { useState } from "react";
import { ToastContainer, toast } from 'react-toastify';

export default function Form({modalActive, setModalActive}){

    const [isSuccessForm, setIsSuccessForm] = useState(false)

    const [name, setName] = useState("")
    const [phone, setPhone] = useState("")
    const [question, setQuestion] = useState("")

    const [errorPhone, setErrorPhone] = useState(false)

    const handleSubmitForm = e => {
        e.preventDefault()

        setErrorPhone(false)

        if(isSuccessForm){
            if(modalActive) setModalActive(false)
            toast.error('Заявка уже была отправлена, попробуйте позже', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }

        if(!phone.length) {
            setErrorPhone(true)
            let res = toast.warn('Введите номер телефона', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });

   
            return
        }


       
        fetchForm()


        console.log(
            name, phone, question
        )
    }


    async function fetchForm(){
        
        let responseForm = await fetch('https://api.styazhka-pola23.ru/wp-json/routes/callback-form', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify({
                name, phone, question
            })
        });
          
        let resultForm = await responseForm.json();
        if(!!resultForm.form_success){
            
            if(modalActive) setModalActive(false)
            toast.dismiss();

            toast.success('заявка отправлена', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });

            setName("")
            setPhone("")
            setQuestion("")
            setIsSuccessForm(true)
        }

        return
        console.log(resultForm)
    }

    return (
        <div className="form">
        
            <form className="form--wrap"
                onSubmit={handleSubmitForm}
            >
                <input 
                    className="form--input" 
                    name="name" 
                    placeholder="Как вас зовут?"
                    value={name}
                    onChange={e => setName(e.target.value) }
                />
                <input 
                    className={errorPhone ? "form--input form--input-error": "form--input"}
                    name="phone" 
                    placeholder="+7 (___) ___-__-__"
                    value={phone}
                    onChange={e => {
                        setPhone(e.target.value)
                        if(errorPhone) setErrorPhone(false)
                    }}
                    
                />
                <textarea 
                    className="form--input form--textarea" 
                    name="question" 
                    placeholder="Кратко опишите задачу"
                    value={question}
                    onChange={e => setQuestion(e.target.value) }
                >
                    {question}
                </textarea>

                <button type="submit" className="form--btn">Оставить заявку</button>
                <FormControlLabel className="form--label" control={
                    <Checkbox 
                        defaultChecked 
                        className="form--checkbox" 
                        sx={{
                            color: "#C4C4C4",
                            '&.Mui-checked': {
                              color: "#000",
                            },
                        }}
                    />
                } label="Я принимаю условия передачи информации" />
            </form>
        </div>
    )
}