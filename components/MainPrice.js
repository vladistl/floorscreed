import Form from "./Form";
import MainPriceList from "./MainPriceList";


export default function MainPrice({priceList}){
    return (
        <div className="main-price">
                <div className="container main-price--container">
                    <p className="title main-price--title" >Цены</p>
                    <div className="main-price--wrap">
                        <div className="main-price--left">
                            <MainPriceList priceList={priceList}/>
                        </div>
                        <div className="main-price--right">
                            <Form/>
                        </div>
                    </div>
                    
                </div>
        </div>
    )
}