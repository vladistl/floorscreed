

export default function PriceList({prices}){



    return (
        <div class="price-list">
            <div className="container price-list--container">
                <div class="price-list--items">

                    {prices.map((item, index) => (
                        <div key={index} class="price-list--item">
                            <div class="price-list--img">
                                <img src={item.img}/>
                            </div>
                            <div class="price-list--name">
                                {item.quadrature}
                            </div>
                            <div class="price-list--value price-list--bold">
                                {item.price}
                            </div>
                        </div>
                    ))}

                

                </div>

        
            </div> 
        </div>
    )
}