import Header from "./Header";

// import mainBackground from ""

export default function FullHeader ({children, menu, phone, setModalActive}) {

    const args = {
        menu, phone, setModalActive
    }
    return (
        <div className="full-header">
            <Header {...args}/>
            {children}
        </div>
    )
}
