import Image from 'next/image'
import Button from '@material-ui/core/Button';


export default function Content({titleContent, img, setModalActive}){
    return (
        <div className="container header-content">
            <div className="header-content--left">
                <h1 className="header-content--title">{titleContent || ""}</h1>
                <Button  className="content-btn"
                     onClick={e => {
                        setModalActive(true)
                    }}
                >
                    <img 
                        className="content-btn--img"
                        src="/img/calculator.png"
                    />
                    <span  className="content-btn--text">
                        Рассчитать стоимость
                    </span>
                </Button>

                <div className="header-content--description">
                    Оставьте заявку и мы<br/> сделаем замеры бесплатно
                    <span className="header-content--description-arrow">
                        <svg xmlns="http://www.w3.org/2000/svg" width="59" height="56" viewBox="0 0 59 56" fill="none">
                            <path d="M54.8536 0.646444C54.6583 0.451181 54.3417 0.451181 54.1464 0.646444L50.9645 3.82842C50.7692 4.02369 50.7692 4.34027 50.9645 4.53553C51.1597 4.73079 51.4763 4.73079 51.6716 4.53553L54.5 1.70711L57.3284 4.53553C57.5237 4.73079 57.8403 4.73079 58.0355 4.53553C58.2308 4.34027 58.2308 4.02369 58.0355 3.82842L54.8536 0.646444ZM54 0.999998C54 30.8234 29.8234 55 -2.18557e-08 55L2.18557e-08 56C30.3757 56 55 31.3757 55 0.999998L54 0.999998Z" fill="white"/>
                        </svg>
                    </span>
                </div>
            </div>
            <div className="header-content--right">
                <img src={img} alt={titleContent || ""} />
            </div>
        </div>
    )
}