import Header from "./Header";

// import mainBackground from ""

export default function MinimalHeader ({contentTitle, menu, phone, setModalActive}) {

    const args = {
        menu, phone, setModalActive
    }

    return (
        <div className="minimal-header">

            <Header  {...args}/>
            <div class="container">
                <h1 className="title">{contentTitle}</h1>
            </div>
        </div>
    )
}
