import Link from 'next/link'
import { useRouter } from 'next/router'
import { useState, useEffect } from "react";


export default function Header ({menu, phone, setModalActive}) {

    const router = useRouter()

    const [toggleMenu, seToggleMenu] = useState(false)
    


    return (
        <>
            <div className="header">
                <div className="container header-container">
                    <Link href="/">
                        <a className="header-logo">
                            <img
                                src="/img/logo.png"
                                alt="Стяжка пола 23"
                        
                                className="header-logo--img"
                            />
                            <span className="header-logo--text">
                                Стяжка пола 23
                            </span>
                        </a>
                    </Link>
                    <div className="menu">
                        {menu.map((item, index) => (
                            <Link key={index} href={item.link}>
                                <a className={ item.isActive? "menu--item active":"menu--item"}>
                                {item.name}
                                </a>
                            </Link>
                        ))}
                    
                    
                    </div>
                    <div className="header-callback">
                        <a className="header-callback--phone" href={`tel:${phone}`}>{phone}</a>
                        <a className="header-callback--btn" 
                            onClick={e => {
                                setModalActive(true)
                            }}
                        >Позвоните мне</a>
                    </div>
                </div>
            </div>
            <div 
                className={toggleMenu ? "header-mobile header-mobile--active" : "header-mobile"}
        
            >
                <div className="header-mobile--top">
                    <Link href="/">
                        <a className="header-mobile--logo">
                            <img
                                src="/img/logo.png"
                                alt="Стяжка пола 23"
                        
                                className="header-mobile--logo--img"
                            />
                            <span className="header-mobile--logo--text">
                                Стяжка пола 23
                            </span>
                        </a>
                    </Link>  
                    <a 
                        onClick={
                            e =>  seToggleMenu(!toggleMenu)
                            
                        }
                    className="header-mobile--toggle-menu">
                        <span className="header-mobile--toggle-menu-text">Меню</span>
                        <span className={toggleMenu ? "header-mobile--toggle-menu-icon-burger header-mobile--toggle-menu-icon-close": "header-mobile--toggle-menu-icon-burger"}></span>
                      
                    </a>
                </div>
                <div className={toggleMenu ? "header-mobile--menu active":"header-mobile--menu" }>
                    <div className="header-mobile--menu-items">
                        {menu.map((item, index) => (
                            <Link key={index} href={item.link}>
                                <a className={ router.pathname == item.link ? "header-mobile--menu--item active":"header-mobile--menu--item"}>
                                {item.name}
                                </a>
                            </Link>
                        ))}
                    </div> 
                    <div className="header-callback">
                        <a className="header-callback--phone" href={`tel:${phone}`}>{phone}</a>
                        <a className="header-callback--btn" 
                            onClick={e => {
                                setModalActive(true)
                            }}
                        >Позвоните мне</a>
                    </div> 
                </div>
            </div>
        </>
    )
}